<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
//header('Access-Control-Allow-Headers: Authorization');
//header('Access-Control-Max-Age: 1');  //1728000
//header("Content-Length: 0");
//header("Content-Type: application/json");

class Auth extends CI_Controller {

	public function doLogin() {

        $this->load->model('Auth_model','auth');
	    $email    = $this->input->post('email');
	    $password = base64_encode($this->input->post('password'));


        $res = $this->auth->admin_login($email, $password);

        if( $res->num_rows() == 1 ) {

            $row = $res->result_array(); // $res convert into associative array

            $last_login_row = $this->auth->last_login($row[0]['id'])->result_array();

            if(count($last_login_row) == 1 ) {

                $response = array(
                    'status'        =>      200,
                    'message'       =>      'success',
                    'id'            =>      (int) $last_login_row[0]['id'],
                    'name'          =>      $last_login_row[0]['name'],
                    'email'         =>      $last_login_row[0]['email'],
                    'password'      =>      base64_decode($last_login_row[0]['password']),
                    'active'        =>      (int) $last_login_row[0]['active'],
                    'admin'         =>      (int) $last_login_row[0]['admin'],
                    'last_login'    =>      $last_login_row[0]['last_login']
                );

                echo json_encode($response);
            }

        }
        else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
  }
}
