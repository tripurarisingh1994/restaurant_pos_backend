<?php
defined('BASEPATH') OR exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');


class Common extends CI_Controller {

    /** Getting Table Category
     * and inside total table
     * */

    public function get_TableCategory_With_TableNo() {
        $this->load->model('Common_model', 'cm');
        $res = $this->cm->fetchTableCategory();

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }


    /** Getting food-category */
    public function getFoodCategory() {
        $this->load->model('Common_model', 'cm');
        $res = $this->cm->fetchFoodCategory();

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Getting the Food Items List by Food Category Id */

    public function getFoodItemsByFoodCatId() {
        $this->load->model('Common_model', 'cm');
        $id = $this->input->get('id');
        $res = $this->cm->fetchFoodItemsByFoodCatId($id);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Getting the price of food items by food id*/

    public function getFoodItemPriceByFoodItemId() {
        $this->load->model('Common_model', 'cm');
        $id  = $this->input->get('id');
        $res = $this->cm->fetchFoodItemPriceByFoodItemId($id);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Saving the Customer Details */

    public function saveCustomersDetails() {

        $this->load->model('Common_model', 'cm');
        $name  = $this->input->post('name');
        $email = $this->input->post('email');
        $mob   = $this->input->post('mob');

        $res = $this->cm->storeCustomerDetails($name, $email, $mob);

        if ($res == 1) {
            echo json_encode( array('status'=>'200','message'=>'success') );
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }

    }

    /** Searching the customer by customer name */
    public function searchCustomerByName() {

        $this->load->model('Common_model', 'cm');
        $query = $this->input->get('query');

        $res = $this->cm->fetchCustomerByName($query);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /**  Search food item by item name */

    public function searchFoodItemsByName() {
        $this->load->model('Common_model', 'cm');
        $query = $this->input->get('query');

        $res = $this->cm->fetchFoodItemsByName($query);

        if ( $res->num_rows()) {
            $rows = $res->result_array();

            $response = array(
                'status'    => 200,
                'message'   => 'success',
                'data'      => $rows
            );

            echo json_encode($response);

        } else {
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }

    /** Insert new Invoice data */
    public function insertInvoiceData() {

        $this->load->model('Common_model', 'cm');
        $customerId   = $this->input->post('customer_id');
        $totalPrice   = $this->input->post('total_price');

        $res = $this->cm->insertInvoice($customerId, $totalPrice);

        if ($res == 1) {
            echo json_encode( array('status'=>'200','message'=>'success') );
        }
        else{
            echo json_encode( array('status'=>'200','message'=>'fail') );
        }
    }
}