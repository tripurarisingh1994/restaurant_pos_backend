<?php

 class Auth_model extends CI_Model {

    public function admin_login($email, $password) {

        $this->db->where('u.email', $email);
        $this->db->where('u.password', $password);
        $this->db->where('u.active', 1);
        $this->db->where('u.admin', 1);
        $this->db->from('users as u');

        return  $this->db->get();
    }

    public function last_login($id) {

        $data = array(
            'last_login' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        $chk = $this->db->update('users', $data);

        if($chk == 1) {
            return $this->db->get_where('users', array('id' => $id));
        }
        else {
            return;
        }
    }
 }
