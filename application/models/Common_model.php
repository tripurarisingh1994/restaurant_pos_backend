<?php

 class Common_model extends CI_Model {

     /** Getting table category and table count
      * */
     public function fetchTableCategory() {
        return $this->db->get('table_category');
     }

     /** Fetching Data From the database food-category */

     public function fetchFoodCategory() {
         return $this->db->get('food_category');
     }

     /** Fetching data from food-item table by food category id*/

     public function fetchFoodItemsByFoodCatId($id) {
        return $this->db->where('food_category',$id)
                 ->get('food_item');
     }

     /** Fetching thr price of food item by food item id*/

     public function fetchFoodItemPriceByFoodItemId($id){
        return $this->db->where('food_item_id', $id)
                    ->get('food_item_rate');
     }

     /** Storing Customer Details in customers table */
     public function storeCustomerDetails($name, $email, $mob) {

         $data = array(
           'name'       =>  $name,
           'email'      =>  $email,
           'mob'        =>  $mob,
           'created_at' =>  date('Y-m-d H:i:s'),
         );

         return $this->db->insert('customers', $data);
     }

     /** Searching the customer by customer name */

     public function fetchCustomerByName($query) {

         return $this->db->like('name', $query)
                          ->from('customers')
                          ->get();

     }

     /** Searching the food items by item name */

     public function fetchFoodItemsByName($query) {

         return $this->db->like('name', $query)
                         ->from('food_item')
                         ->get();
     }

     /** Insert new Invoice Data */

     public function insertInvoice($customerId, $totalPrice) {

         $data = array(
             'customer_id'  =>  $customerId,
             'total_price'  =>  $totalPrice,
             'created_at'   =>  date('Y-m-d H:i:s'),
         );

         return $this->db->insert('invoice', $data);
    }
 }